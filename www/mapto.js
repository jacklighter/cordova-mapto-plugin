var exec = require('cordova/exec');
/**
 * Provides access to the preferred mapping application.
 * Defaults to Apple Maps on iOS
 */
module.exports = {
    /**
     * Launches the preferred mapping applicaiton and displays the location of the address.
     *
     * @param {string} address		The address to display.
     */
    mapTo: function(address) {
        exec(null, null, "MapTo", "mapTo", [address]);
    },

    /**
     * Launches the preferred mapping applicaiton and navigates to the location of the address.
     *
     * @param {string} address		The address to navigate to.
     */
    navigateTo: function(address) {
        exec(null, null, "MapTo", "navigateTo", [address]);
    },
};