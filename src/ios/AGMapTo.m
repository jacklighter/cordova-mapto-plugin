//
//  AGMapTo.m
//
//  Created by Jacklighter, Inc. on 4/11/14.
//
//

#import "AGMapTo.h"

@interface AGMapTo()
- (void)launchMapsForAddress:(NSString *)address isNavigation:(BOOL)isNavigation;
- (NSURL *)appleMapsURLForAddress:(NSString *)address isNavigation:(BOOL)isNavigation;
- (NSURL *)googleMapsURLForAddress:(NSString *)address isNavigation:(BOOL)isNavigation;
@end

@implementation AGMapTo

#pragma mark -Public Methods
- (void)mapTo:(CDVInvokedUrlCommand *)command
{
    if ( command.arguments.count != 1 ) {
        return;
    }

    NSString *address = command.arguments[0];
    [self launchMapsForAddress:address isNavigation:NO];
}

- (void)navigateTo:(CDVInvokedUrlCommand *)command
{
    if ( command.arguments.count != 1 ) {
        return;
    }

    NSString *address = command.arguments[0];
    [self launchMapsForAddress:address isNavigation:YES];
}

#pragma mark -Private Methods
- (NSString *)defaultMappingApp
{
    NSString *mapSetting = [[NSUserDefaults standardUserDefaults] stringForKey:@"maps_application"];
    return (mapSetting) ? mapSetting : @"apple";
}

- (void)launchMapsForAddress:(NSString *)address isNavigation:(BOOL)isNavigation
{
    NSString *app = [self defaultMappingApp];
    NSURL *googleMapsSchema = [NSURL URLWithString:@"comgooglemaps://"];
    NSURL *mapURL;

    if ( [app isEqualToString:@"google"] && [[UIApplication sharedApplication] canOpenURL:googleMapsSchema] ) {
        mapURL = [self googleMapsURLForAddress:address isNavigation:isNavigation];
    } else {
        mapURL = [self appleMapsURLForAddress:address isNavigation:isNavigation];
    }

    [[UIApplication sharedApplication] openURL:mapURL];
}

- (NSURL *)appleMapsURLForAddress:(NSString *)address isNavigation:(BOOL)isNavigation
{
    NSString *urlString;

    address = [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if ( isNavigation ) {
        urlString = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=Current+Location&daddr=%@", address];
    } else {
        urlString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@", address];
    }
    
    return [NSURL URLWithString:urlString];
}

- (NSURL *)googleMapsURLForAddress:(NSString *)address isNavigation:(BOOL)isNavigation
{
    NSString *urlString;
    
    address = [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if ( isNavigation ) {
        urlString = [NSString stringWithFormat:@"comgooglemaps://?daddr=%@&directionsmode=driving", address];
    } else {
        urlString = [NSString stringWithFormat:@"comgooglemaps://?q=%@", address];
    }
    
    return [NSURL URLWithString:urlString];
}


@end
