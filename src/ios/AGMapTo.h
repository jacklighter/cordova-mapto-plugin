//
//  AGMapTo.h
//
//  Created by Jacklighter, Inc.
//
//

#import <Cordova/CDVPlugin.h>

@interface AGMapTo : CDVPlugin
- (void)mapTo:(CDVInvokedUrlCommand *)command;
- (void)navigateTo:(CDVInvokedUrlCommand *)command;
@end
