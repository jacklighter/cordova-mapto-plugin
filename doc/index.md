# com.agendafleet.maps

This plugin provides a way to vibrate the device.

## Installation

    cordova plugin add https://bitbucket.org/jacklighter/cordova-mapto-plugin.git

## Supported Platforms

- iOS

## mapTo

Launches the preferred mapping applicaiton and displays the location of the address.

    navigator.mapTo(address)

- __address__: Address to display _(String)_


### Example

    // Open map of the White House
    navigator.mapTo('1600 Pennsylvania Avenue NW, Washington, D.C.');

## navigateTo
Launches the preferred mapping applicaiton and navigates to the location of the address.

    navigator.navigateTo(address)

- __address__: Address to navigate to _(String)_


### Example

    // Open directions to the White House
    navigator.navigateTo('1600 Pennsylvania Avenue NW, Washington, D.C.');


